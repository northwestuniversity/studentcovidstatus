package nwu.student;

import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.UI;
import nwu.ac.za.framework.SinglePageCRUDUI;
import nwu.ac.za.framework.VaadinUIUtility;
import nwu.ac.za.framework.context.SessionContext;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.events.ClearErrorEvent;
import nwu.student.ui.components.StudentCovidStatusComponent;
import nwu.student.ui.config.ApplicationConfig;
import nwu.student.ui.model.dto.ExecutionContext;
import nwu.student.ui.service.PersonProxyService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 * <p>
 * Created by Henriko on 2018/03/29.
 */
@Theme("nwu-default")
@SpringUI
public class StudentCovidStatusCrudUI extends SinglePageCRUDUI {

    private ExecutionContext executionContext;
    private final Logger log = LoggerFactory.getLogger(StudentCovidStatusCrudUI.class.getName());

	private boolean isStudent = false;

    @Autowired
    private ApplicationConfig appConfig;
    @Autowired
    private StudentCovidStatusComponent studentCovidStatusComponent;
    @Autowired
    private EventBus eventBus;
    @Autowired
    private VaadinI18n vaadinI18n;

    @Autowired
	private PersonProxyService personProxyService;

    @Override
    public String getBrowserTabTitle() {
        return "student-covid-status";
    }

    @Override
    public AbstractLayout getDetailContent() {
        initialize();
        studentCovidStatusComponent.initUI(executionContext);
        studentCovidStatusComponent.eventListeners();
        return studentCovidStatusComponent;
    }

    @Override
    public void afterUISetup() {
    }

	private void initialize() {
		overrideAuthUserBasedOnConfig();
		lookupUser = this.authenticatedUser;
		setExecutionContextAndAuthentication();
		determineIsStudent();
		eventBus.register(this);
	}

    private void overrideAuthUserBasedOnConfig() {
        // For development puposes check if there is another DEV_INITIAL_UNIV_NUMBER specified and only set it if CAS is not on
        if (appConfig.getCaseEnabled().toUpperCase().equals("FALSE")) {
            String devInintialUnivNumber = appConfig.getDevInitialUnivNumber();
            if (devInintialUnivNumber != null) {
                lookupUser = devInintialUnivNumber;
                authenticatedUser = devInintialUnivNumber;
                //Lookup user are not updated at the correct time.
                getSessionContext().setLookupUser(lookupUser);
                getSessionContext().setAuthenticatedUser(authenticatedUser);
            }
        }
    }

	public void determineIsStudent() {
		List<PersonAffiliationInfo> personAffiliationInfos = new ArrayList<>();
		personAffiliationInfos = personProxyService.getPersonAffiliation(executionContext.getLookupUser());

		if (personAffiliationInfos != null && personAffiliationInfos.size() > 0) {
			for (PersonAffiliationInfo userRole : personAffiliationInfos) {
				if (userRole.getAffiliationTypeKey().toLowerCase().contains("vss.code.hoedanig.s")) {
					isStudent = true;
				}
			}
		}

		if(!isStudent){
			log.error("University number: " + executionContext.getLookupUser() + " is not a student");
			throw new VaadinUIException(vaadinI18n.getMessage("uniNumber.not.student"));
		}
	}

    public void setExecutionContextAndAuthentication() {
        try {
            log.info("setExecutionContextAndAuthentication...");
            executionContext = new ExecutionContext();
            executionContext.setUserAuthenticated(true);
            executionContext.setLookupUser(lookupUser);
            executionContext.setAuthenticatedUser(lookupUser); // authenticatedUser
            // lookupUser
            Locale uiLocale = UI.getCurrent().getLocale();
            if (uiLocale.getLanguage().equals(VaadinUIUtility.SYSTEM_LANG_AF)) {
                executionContext.setSystemLanguageTypeKey(VaadinUIUtility.TYPE_KEY_SYSTEM_LANGUAGE_AF);
            } else {
                executionContext.setSystemLanguageTypeKey(VaadinUIUtility.TYPE_KEY_SYSTEM_LANGUAGE_EN);
            }
        } catch (Exception ex) {
            log.error("There was a problem with setting execution context: " + ex);
            throw new VaadinUIException("There was a problem with setting execution context: " + ex);
        }
    }

    public ExecutionContext getExecutionContext() {
        return executionContext;
    }

    public void setExecutionContext(ExecutionContext executionContext) {
        this.executionContext = executionContext;
    }

    @Override
    public void save() {
    }

    @Override
    public SessionContext newSessionContext() {
        return new SessionContext();
    }

    @Subscribe
    public void clearUIErrors(ClearErrorEvent errorEvent) {
        this.clearErrors();
    }

    public static StudentCovidStatusCrudUI getCurrent() {
        return (StudentCovidStatusCrudUI) UI.getCurrent();
    }

}
