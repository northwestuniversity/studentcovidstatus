package nwu.student.ui.service;

import assemble.edu.exceptions.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.document.dto.DocumentInfo;
import nwu.enabling.capabilities.supporting.service.document.dto.DocumentServiceConnectionInfo;
import nwu.enabling.capabilities.supporting.service.document.service.crud.DocumentServiceCRUD;
import nwu.enabling.capabilities.supporting.service.document.service.crud.factory.DocumentServiceCRUDClientFactory;
import nwu.student.ui.config.ApplicationConfig;
import nwu.student.ui.config.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.io.IOException;

/*
 Created by Henriko on 2020/12/08 
*/
@Service
@RefreshScope
public class DocumentManagementCRUDServiceProxy extends AbstractServiceProxy<DocumentServiceCRUD> {

    @Autowired
    private Config config;

    @Autowired
    private ApplicationConfig appConfig;

    @Autowired
    private KeyStoreManager keyStoreManager;

    private DocumentServiceConnectionInfo documentServiceConnectionInfo;

    public DocumentManagementCRUDServiceProxy() {
    }

    @Override
    protected DocumentServiceCRUD initService() throws Exception {
        documentServiceConnectionInfo = new DocumentServiceConnectionInfo();
        documentServiceConnectionInfo.setDocumentServiceUserGroup(appConfig.getDocumentServiceUserGroup());

        documentServiceConnectionInfo.setDocumentServiceUserGroupPassword(keyStoreManager.decrypt(
                appConfig.getDocumentServiceUserPassword()));

        String db = config.getWebServiceDatabase();
        String environment = config.getRuntimeEnvironment();
        String version = ServiceRegistryLookupUtility.getMajorVersion(config.getDocumentManagementAPIVersion());

        String lookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(DocumentServiceCRUDClientFactory.DOCUMENTSERVICECRUD,
                version, db, environment);

        String username = config.getDocumentServiceCrudUsername();
        String password = keyStoreManager.decrypt(config.getDocumentServiceCrudPassword());

        return DocumentServiceCRUDClientFactory.getDocumentServiceCRUD(lookupKey, username, password);
    }

    public DocumentInfo createDocument(DocumentInfo documentInfo, byte[] actualDocument, String systemLanguageTypeKey) {

        try {
            ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();

            try {
                log.info("Request to DocumentServiceCRUD method createDocument: " +
                        "documentInfo : " + writer.writeValueAsString(documentInfo) + "\n" +
                        "actualDocument size: " + actualDocument.length + "\n" +
                        "Lang: " + systemLanguageTypeKey);

            } catch (IOException e) {
                log.warn("Could not map request documentInfo to string exception: " + e);
            }

            return getService().createDocument(documentInfo, actualDocument, systemLanguageTypeKey,
                    documentServiceConnectionInfo, getContextInfo(null));

        } catch (DoesNotExistException e) {
            log.error("Could not create document: " + e.getMessage(), e);
            throw new VaadinUIException("service.error.create.document.not.found", true, e);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen exception while creating document: Reference: 100101: " + e.getMessage(), e);
            throw new VaadinUIException("service.error.create.document.fails", true, e);

        } catch (Exception e) {
            log.error("Unforeseen exceptiDocumentManagementServiceProxyon while creating document: Reference: 100102: " + e.getMessage(), e);
            throw new VaadinUIException("service.error.create.document.exception", true, e);
        }
    }

    public void deleteDocument(String documentId) {
        try {
            getService().deleteDocument(documentId, documentServiceConnectionInfo, getContextInfo(null));
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(e.getMessage(), e);
        }
    }
}
