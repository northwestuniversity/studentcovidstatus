package nwu.student.ui.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.document.dto.DocumentInfo;
import nwu.enabling.capabilities.supporting.service.document.dto.DocumentKeywordInfo;
import nwu.enabling.capabilities.supporting.service.document.dto.DocumentServiceConnectionInfo;
import nwu.enabling.capabilities.supporting.service.document.service.DocumentService;
import nwu.enabling.capabilities.supporting.service.document.service.factory.DocumentServiceClientFactory;
import nwu.student.ui.config.ApplicationConfig;
import nwu.student.ui.config.Config;

@Service
@RefreshScope
public class DocumentManagementServiceProxy extends AbstractServiceProxy<DocumentService> {

    @Autowired
    private ApplicationConfig appConfig;
    @Autowired
    private Config config;
    @Autowired
    private KeyStoreManager keyStoreManager;

    private DocumentServiceConnectionInfo documentServiceConnectionInfo;

    public DocumentManagementServiceProxy() {}

    @Override
    protected DocumentService initService() {
        documentServiceConnectionInfo = new DocumentServiceConnectionInfo();
        documentServiceConnectionInfo.setDocumentServiceUserGroup(appConfig.getDocumentServiceUserGroup());
        documentServiceConnectionInfo.setDocumentServiceUserGroupPassword(keyStoreManager.decrypt(appConfig.getDocumentServiceUserPassword()));
//        documentServiceConnectionInfo.setDocumentServiceUserGroup("ONBASE.CONFIG.DOCUMENTTYPE.Covid Vaccination Certificate");

        try {
            String db = config.getWebServiceDatabase();
            String environment = config.getRuntimeEnvironment();
            String version = ServiceRegistryLookupUtility.getMajorVersion(config.getDocumentManagementAPIVersion());
            String lookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(DocumentServiceClientFactory.DOCUMENTSERVICE,
                    version, db, environment);

            String username = config.getDocumentServiceUsername();
            String password = keyStoreManager.decrypt(config.getDocumentServicePassword());

            return DocumentServiceClientFactory.getDocumentService(lookupKey, username, password);

        } catch (Exception ex) {
            log.error("Could not initiate DocumentService : " + ex.getMessage(), ex);
            throw new VaadinUIException("service.error.document.init", ex);
        }
    }
    

    public List<DocumentInfo> getDocumentsBySearchCriteria(String documentType, HashMap<String, String> documentKeywords,
            Integer maxReturningDocuments, String systemLanguageTypeKey) {

        try {

            log.info("Request to DocumentService method getLatestDocumentByDocumentTypeAndKeyword: " +
                    "documentType : " + documentType + "\n" +
                    "documentKeywords size: " + documentKeywords.size() + "\n" +
                    "maxReturningDocuments: " + maxReturningDocuments + "\n" +
                    "systemLanguageTypeKey: " + systemLanguageTypeKey);


            return getService().getDocumentsBySearchCriteria(null, null, documentType, documentKeywords, maxReturningDocuments, systemLanguageTypeKey, documentServiceConnectionInfo,
                    new ContextInfo(config.getAppName()));
            		

        } catch (DoesNotExistException e) {
        	// Ignore this as it is valid that there might not be any documents
//            log.error("Could not document by DocumentType and keyword: " + e.getMessage(), e);
//            throw new VaadinUIException("service.error.latest.document.not.found", e);
        	return null;

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen exception while finding document by DocumentType and keyword: Reference: 100500: " + e.getMessage(), e);
            throw new VaadinUIException("service.error.latest.document.fails", e);

        } catch (Exception e) {
            log.error("Unforeseen exception while finding document by DocumentType and keyword: Reference: 100600: " + e.getMessage(), e);
            throw new VaadinUIException("service.error.latest.document.exception", e);

        }
	}
    
    public DocumentInfo getLatestDocumentByDocumentTypeAndKeyword(String documentType, HashMap<String, String> documentKeywords,
                                                                  Integer maxReturningDocuments, String systemLanguageTypeKey) {

        try {

            log.info("Request to DocumentService method getLatestDocumentByDocumentTypeAndKeyword: " +
                    "documentType : " + documentType + "\n" +
                    "documentKeywords size: " + documentKeywords.size() + "\n" +
                    "maxReturningDocuments: " + maxReturningDocuments + "\n" +
                    "systemLanguageTypeKey: " + systemLanguageTypeKey);


            return getService().getLatestDocumentByDocumentTypeAndKeyword(documentType, documentKeywords,
                    maxReturningDocuments, systemLanguageTypeKey, documentServiceConnectionInfo,
                    new ContextInfo(config.getAppName()));

        } catch (DoesNotExistException e) {
            log.error("Could not find latest document by DocumentType and keyword: " + e.getMessage(), e);
            throw new VaadinUIException("service.error.latest.document.not.found", e);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen exception while finding latest document by DocumentType and keyword: Reference: 100500: " + e.getMessage(), e);
            throw new VaadinUIException("service.error.latest.document.fails", e);

        } catch (Exception e) {
            log.error("Unforeseen exception while finding latest document by DocumentType and keyword: Reference: 100600: " + e.getMessage(), e);
            throw new VaadinUIException("service.error.latest.document.exception", e);

        }
    }

    public byte[] exportActualDocumentByID(String actualDocumentID) {
        try {
            log.info("Calling DocumentService on method exportActualDocumentByID: \n" +
                    "actualDocumentID: " + actualDocumentID);

            return getService().exportActualDocumentByID(actualDocumentID, documentServiceConnectionInfo,
                    new ContextInfo(config.getAppName()));

        } catch (DoesNotExistException e) {
            log.error("File not found to exportActualDocumentByID: " + e.getMessage(), e);
            throw new VaadinUIException("service.document.export.not.found", e);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen exception occurred while exportActualDocumentByID: Reference 100201: " + e.getMessage(), e);
            throw new VaadinUIException("service.document.export.fails", e);

        } catch (Exception e) {
            log.error("Unforeseen exception occurred while exportActualDocumentByID: Reference 100202: " + e.getMessage(), e);
            throw new VaadinUIException("service.document.export.exception", e);
        }
    }

    public List<DocumentKeywordInfo> getDocumentTypesByUserGroup(String systemLanguage) {
        try {

            documentServiceConnectionInfo = new DocumentServiceConnectionInfo();
//            documentServiceConnectionInfo.setDocumentServiceUserGroup(appConfig.getDocumentServiceUserGroup());
//            documentServiceConnectionInfo.setDocumentServiceUserGroupPassword(keyStoreManager.decrypt(appConfig.getDocumentServiceUserPassword()));
            // Debugging
            documentServiceConnectionInfo.setDocumentServiceUserGroup("COVID-USER");
            documentServiceConnectionInfo.setDocumentServiceUserGroupPassword("COVID@ONBASE");
//            documentServiceConnectionInfo.setDocumentServiceUserGroup("ONBASE.CONFIG.DOCUMENTTYPE.Covid Vaccination Certificate");

            return getService().getDocumentTypesByUserGroup(systemLanguage, documentServiceConnectionInfo, getContextInfo(config.getAppName()));

        } catch (DoesNotExistException e) {
            log.error("Could not find documentType by group: " + e.getMessage(), e);
            throw new VaadinUIException("service.document.type.group.not.found", e);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen exception while finding documentType by group: Reference number 10100: " + e.getMessage(), e);
            throw new VaadinUIException("service.document.type.group.exception", e);

        } catch (Exception e) {
            log.error("Unforeseen exception while finding documentType by group: Reference number 10200: " + e.getMessage(), e);
            throw new VaadinUIException("service.document.type.group.exception", e);
        }
    }
}