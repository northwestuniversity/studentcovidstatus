package nwu.student.ui.service;

import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.*;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.permission.service.PermissionService;
import nwu.enabling.capabilities.supporting.service.permission.service.factory.PermissionServiceClientFactory;
import nwu.student.ui.config.ApplicationConfig;
import nwu.student.ui.config.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 Created by Henriko on 2020/12/15 
*/
@Service
@RefreshScope
public class PermissionProxyService extends AbstractServiceProxy<PermissionService> {
    @Autowired
    private Config config;

    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected PermissionService initService() throws Exception {

        try {
            String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getSupportingServicesVersion());
            String appRuntimeEnv = config.getRuntimeEnvironment();

            String permissionServiceLookupKey = getServiceRegistryLookupKey(
                    PermissionServiceClientFactory.PERMISSIONSERVICE,
                    wsMajorVersion + "/",
                    config.getWebServiceDatabase(),
                    appRuntimeEnv);

            log.info("Permission service lookupKey: " + permissionServiceLookupKey);
            String decryptedPassword = keyStoreManager.decrypt(config.getSupportingServicesReadPassword());

            return PermissionServiceClientFactory.getPermissionService(
                    permissionServiceLookupKey,
                    config.getSupportingServicesReadUsername(),
                    decryptedPassword);
        } catch (Exception e) {
            log.error("Could not initialize Permission service: " + e.getMessage(), e);
            throw new VaadinUIException("error.permission.service.init", e);
        }
    }

    public String getMembersInGroup(String groupName, String groupTypeKey) {
        try {
            String results = "";
            List<String> response = getService().getMembersInGroup(groupName, groupTypeKey,
                    new ContextInfo(config.getAppName()));

            results = response.toString().replace("[", "")
                    .replace("]", "").replace(", ", ",");

            log.error("Grouper Results :" +results);

            //The last result from the service are "jboss-jbpm" this code it to remove that words.
            String[] resultArray = results.split(",");
            String universityNumbers = "";
            if(resultArray.length > 0) {
                int counter = 1;
                for(String item:resultArray) {
                    counter++;

                    try {
                        int universityNumber = Integer.parseInt(item);
                        universityNumbers = universityNumbers.concat(Integer.toString(universityNumber));
                    } catch (NumberFormatException numEx) {
                        log.warn("Could not convert String to Integer: " + item + ". Error: " + numEx.getMessage(), numEx);
                    }

                    if(counter < resultArray.length ) {
                        universityNumbers = universityNumbers.concat(",");
                    }
                }
            }
            log.info("UniversityNumbers: " + universityNumbers);
            return universityNumbers;

        } catch (DoesNotExistException e) {
            log.error("Could not find members in group: " + e.getMessage());
            throw new VaadinUIException("error.permission.service.members.not.found", e);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Unforeseen error when getting members from group: Reference number 100 :" + e.getMessage());
            throw new VaadinUIException("error.permission.service.members.fails", e);

        } catch (Exception e) {
            log.error("Unforeseen exception when getting members from group: Reference number 200:  " + e.getMessage());
            throw new VaadinUIException("error.permission.service.members.exception" + e.getMessage(), e);
        }
    }
}
