package nwu.student.ui.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ac.za.nwu.core.type.dto.TypeAttributeValueInfo;
import ac.za.nwu.core.type.dto.TypeInfo;
import ac.za.nwu.core.type.service.TypeService;
import ac.za.nwu.registry.utility.GenericServiceClientFactory;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.ui.config.ApplicationConfig;
import nwu.student.ui.config.Config;

@Service
public class TypeProxyService extends AbstractServiceProxy<TypeService> {

    ApplicationConfig applicationConfig;

    @Autowired
    private Config config;
    
    @Autowired
    private KeyStoreManager keyStoreManager;


    
    public TypeProxyService(ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
    }

    @Override
    protected TypeService initService() throws Exception {
    	String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getIdentityApiVersion());
    	String appRuntimeEnv = config.getRuntimeEnvironment();
        String typeServiceKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                TypeService.TYPESERVICE,
                wsMajorVersion,
                null,
                appRuntimeEnv);

        try {
        	String decryptedPassword = keyStoreManager.decrypt(config.getIApisReadPassword());
            return (TypeService)GenericServiceClientFactory.getService(
            		typeServiceKey, config.getIApisReadUsername(), decryptedPassword, TypeService.class );

        } catch (Exception ex) {
            log.error("Could not initialize Type service: " + ex);
            throw new VaadinUIException("Could not initialize Type service.");
        }
    }

    public TypeInfo getType(String typeKey) {
        try {
            TypeInfo respone = getService().getType(typeKey, new ContextInfo(config.getAppName()));

            return respone;

        } catch (DoesNotExistException | InvalidParameterException | OperationFailedException |
                MissingParameterException | PermissionDeniedException e) {

            log.error("Could not get type information from key: " + typeKey + " : " + e);

            throw new VaadinUIException("error.type.service.gettype",true);
        }
    }

    public List<TypeAttributeValueInfo> getTypeAttributeValues(String typeKey, String catAttTypeKey) {
        try {

            List<TypeAttributeValueInfo> results = getService().getTypeAttributeValues(
            		typeKey,
            		catAttTypeKey,
            		new ContextInfo(config.getAppName()));

            return results;

        } catch (DoesNotExistException e1) {
            log.warn("Could not get type attribute values. " + e1);
        	//Ignoring the error
            return null;
        } catch (InvalidParameterException |
                OperationFailedException |
                MissingParameterException |
                PermissionDeniedException e) {

            log.error("Could not get type attribute values. " + e);
            throw new VaadinUIException("error.type.service.type.attribute.fail",true);
        }
    }

    public List<TypeInfo> getTypesByCategory(String category, String language) {
        try {
            return getService().getTypesByCategory(category, language, new ContextInfo(config.getAppName()));

        } catch (DoesNotExistException e) {
            log.error("Could not find Types By category: " + e.getMessage(), e);
            throw new VaadinUIException("error.typeservice.not.found", true);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Exception occurred while getting Types By category: " + e.getMessage(), e);
            throw new VaadinUIException("error.typeservice.error", true);

        } catch (Exception e) {
            log.error("Unexpected Exception occurred while getting Types By category: " + e.getMessage(), e);
            throw new VaadinUIException("error.typeservice.failed", true);
        }
    }
}