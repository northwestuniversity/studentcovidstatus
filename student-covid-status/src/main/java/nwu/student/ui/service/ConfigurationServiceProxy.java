package nwu.student.ui.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nwu.ac.za.framework.auth.KeyStoreManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ac.za.nwu.utility.EncryptorUtility;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.configuration.service.ConfigurationService;
import nwu.enabling.capabilities.supporting.service.configuration.service.factory.ConfigurationServiceClientFactory;
import nwu.student.ui.config.Config;

@Service
public class ConfigurationServiceProxy extends AbstractServiceProxy<ConfigurationService>  {
    public static final Logger log = LoggerFactory.getLogger(ConfigurationServiceProxy.class);

    @Autowired
    private Config config;

    @Autowired
    private KeyStoreManager keyStoreManager;

//    @Autowired
//    private KeyStoreManager keyStoreManager;
    
    private static final String VERSION_GLOBAL = "GLOBAL";

    private String appName;
    private String appMajorVersion;
    private String wsMajorVersion;
    private String appRuntimeEnv;
    private String password;
    private String username;
    private String database;
    
    protected ConfigurationService initService() throws Exception {
        String configAPIVersion = config.getConfigurationManagmentApiVersion();
        this.wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(configAPIVersion);
        String applicationVersion = config.getAppVersion();
        this.appMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(applicationVersion);      
    	this.appName = config.getAppName();
    	this.appRuntimeEnv = config.getRuntimeEnvironment();
    	this.username = config.getSupportingServicesReadUsername();
    	this.password = config.getSupportingServicesReadPassword();
    	this.database = config.getWebServiceDatabase();
    	
          
        String lookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(ConfigurationServiceClientFactory.CONFIGURATIONSERVICE, wsMajorVersion, database, appRuntimeEnv);

        try {
            String decryptedPassword = keyStoreManager.decrypt(password);
            return ConfigurationServiceClientFactory.getConfigurationService(lookupKey, username, decryptedPassword);
        } catch (Exception ex) {
            log.error("Could not initialize Config service: " + lookupKey + ex);
            throw new VaadinUIException("Could not initialize Config service: " + lookupKey + ex, true);
        }
    }

    public Map<String,String> getApllicationVersionSpecificConfiguration() {
        try {

            ContextInfo contextInfo = new ContextInfo();
            contextInfo.setCurrentDate(new Date());
            contextInfo.setPrincipalId(null);
            contextInfo.setSubscriberToken("");
            contextInfo.setSubscriberClientName(appName);

            HashMap<String, String> configValues = getService().getConfigurationByApplication(appRuntimeEnv, appName, appMajorVersion, contextInfo);
            return configValues;
        } catch (DoesNotExistException e) {
            log.info("There are no Configuration for the application" + appName + " " + appMajorVersion);
            return null;
        } catch (InvalidParameterException | MissingParameterException
                | OperationFailedException e1) {
            String exp = "Reading of Application Configuration Failed" + appName + " " + appMajorVersion;
            throw new RuntimeException(exp, e1);
        }
    }

    public Map<String,String> getApplicationGlobalConfiguration() {
        try {

            ContextInfo contextInfo = new ContextInfo();
            contextInfo.setCurrentDate(new Date());
            contextInfo.setPrincipalId(null);
            contextInfo.setSubscriberToken("");
            contextInfo.setSubscriberClientName(appName);

            return getService().getConfigurationByApplication(appRuntimeEnv, appName, VERSION_GLOBAL, contextInfo);
        } catch (DoesNotExistException e) {
            String exp = "There are no Global Configuration for the application " + appName + " " + appMajorVersion;
            throw new RuntimeException(exp, e);
        } catch (InvalidParameterException | MissingParameterException
                | OperationFailedException e1) {
            String exp = "Reading of Application Global Configuration Failed" + appName + " " + appMajorVersion;
            throw new RuntimeException(exp, e1);
        }

    }
}
