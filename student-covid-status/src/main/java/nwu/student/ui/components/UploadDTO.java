package nwu.student.ui.components;

public class UploadDTO {

	private static final long serialVersionUID = 1L;
	boolean vaccinated = false;
	boolean isScanQRCode = false;
	boolean isDisplayDoc = false;
	boolean isMandatoryDoc = false;
	String onbaseDocTipe = null;
	String vaccinationTypeMap = null;

	public UploadDTO(boolean vaccinated2, boolean isDisplayDoc2, boolean isMandatoryDoc2, boolean isScanQRCode2,
			String onbaseDocTipe2, String vaccinationTypeMap2) {
		System.out.println("Doc Tipe : " + onbaseDocTipe2 + " : " + isMandatoryDoc2) ;
		this.vaccinated = vaccinated2;
		this.isDisplayDoc = isDisplayDoc2;
		this.isMandatoryDoc = isMandatoryDoc2;
		this.isScanQRCode = isScanQRCode2;
		this.onbaseDocTipe = onbaseDocTipe2;
		this.vaccinationTypeMap = vaccinationTypeMap2;
	}

}
