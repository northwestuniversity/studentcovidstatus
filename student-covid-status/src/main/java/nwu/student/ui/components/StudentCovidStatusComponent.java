package nwu.student.ui.components;

import ac.za.nwu.common.dto.MetaInfo;
import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.person.dto.PersonVaccinactionInfo;
import ac.za.nwu.core.type.dto.TypeAttributeValueInfo;
import ac.za.nwu.core.type.dto.TypeInfo;
import assemble.edu.exceptions.*;
import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.gwt.user.client.ui.RadioButton;
import com.vaadin.data.HasValue;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import nwu.ac.za.framework.i18n.Translate;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.ui.utils.DateUtils;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.document.dto.DocumentInfo;
import nwu.student.StudentCovidStatusCrudUI;
import nwu.student.events.ClearErrorEvent;
import nwu.student.events.UploadEvent;
import nwu.student.ui.config.ApplicationConfig;
import nwu.student.ui.config.Config;
import nwu.student.ui.designs.MainViewDesign;
import nwu.student.ui.dto.OnbaseContainer;
import nwu.student.ui.dto.OnbaseFileLinkButton;
import nwu.student.ui.dto.OnbaseFileLinkButtonForGrid;
import nwu.student.ui.dto.VaccinationDocumentHistory;
import nwu.student.ui.model.dto.ExecutionContext;
import nwu.student.ui.service.*;
import nwu.student.ui.util.StudentCovidStatusConstants;
import nwu.student.ui.util.UIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.Map.Entry;


@SpringComponent
@UIScope
@Translate
@RefreshScope
public class StudentCovidStatusComponent extends MainViewDesign {

	private static final String PERSON_VACCINATION_COVID = ".1";
	private static final String PERSON_VAC_STATUS_FULLYVACINATED = ".1";
	private static final String PERSON_VAC_STATUS_UNVACINATED = ".3";
	private final Logger log = LoggerFactory.getLogger(StudentCovidStatusComponent.class.getName());
	public static final String DOCUMENT_TYPE_COVID_VACCINATION_CERTIFICATE = "ONBASE.CONFIG.DOCUMENTTYPE.Covid Vaccination Certificate";
	private final String GROUPER_KEY = "vss.code.CodeIndex.GROUPTYPEKEY_NWU_GROUPER";

	@Autowired
	private PersonProxyService personProxyService;
	@Autowired
	private ApplicationConfig appConfig;
	@Autowired
	private Config config;
	@Autowired
	private DocumentManagementServiceProxy documentManagementServiceProxy;
	@Autowired
	private DocumentManagementCRUDServiceProxy documentManagementCRUDServiceProxy;
	@Autowired
	private VaadinI18n vaadinI18n;
	@Autowired
	private EventBus eventBus;
	@Autowired
	private PermissionProxyService permissionProxyService;

	@Autowired
	private TypeProxyService typeService;

	private HashMap<String, OnbaseContainer> filesThatNeedsToBeUploadedToOnbase;
	private HashMap<String, UploadComponent> uploadComponentMap;
//	private OnbaseContainer covidVacCertOnbaseContainer;
//	private UploadComponent covidVacCertUploadComponent;
	private String systemLanguage;
	private PersonBiographicInfo personBiographic;
	private ExecutionContext executionContext;
	private Boolean authUserInGroup = false;

	private Grid<VaccinationDocumentHistory> vaccinationDocumentHistoryGrid = new Grid<>();
	private List<VaccinationDocumentHistory> vaccinationDocumentHistoryList;
	private ListDataProvider<VaccinationDocumentHistory> vaccinationDocumentHistoryListDataProvider;
	private boolean isVaccinatedAgainstCovid;
	private PersonVaccinactionInfo vacInfo;
	private VerticalLayout uploadVL;
	private HashMap<String, UploadDTO> docTipesForVaccinated;
	private HashMap<String, UploadDTO> docTipesForUnVaccinated;
	private ComboBox<TypeInfo> vacTypeComboBox;
	

	@PostConstruct
	private void init() {
		disclaimerChkBox.setCaptionAsHtml(true);

		if (UI.getCurrent().getLocale().getLanguage().contains("af")) {
			systemLanguage = "vss.code.Language.2";
		} else {
			systemLanguage = "vss.code.Language.3";
		}
	}

	public void initUI(ExecutionContext executionContext) {
		this.executionContext = executionContext;
		doUploadLayout.setVisible(false);

		if (appConfig.getAdminGroup() != null) {
			String membersInGroup = permissionProxyService.getMembersInGroup(appConfig.getAdminGroup(), GROUPER_KEY);
			authUserInGroup = !Strings.isNullOrEmpty(membersInGroup)
					&& membersInGroup.contains(executionContext.getAuthenticatedUser());
		}

		if (config.getRuntimeEnvironment().toUpperCase().contains("QA")) {
			setUniversityNumberTextfieldVisible(authUserInGroup || appConfig.getUniversitynumberAllowed());
		} else {
			// TEST OF PROD
			setUniversityNumberTextfieldVisible(authUserInGroup);
		}

		clearUserData();
		loadInitialData(true);
		eventBus.register(this);
	}

	public void documentTypesByVacStatus(String vacStatus) {

		// Get all the values for COVIDSTATUSAP
		List<TypeInfo> allDocs = typeService.getTypesByCategory("vss.code.COVIDSTATUSAP", this.executionContext.getSystemLanguageTypeKey());

		// For each value check what is the QG value Y / N
		docTipesForVaccinated = new HashMap<String, UploadDTO>();
		docTipesForUnVaccinated = new HashMap<String, UploadDTO>();

		List<TypeInfo> vaccinationTypeList = new ArrayList<TypeInfo>();
		HashMap<String, TypeInfo> vaccinationTypeInfoMap = new HashMap<String, TypeInfo>();
		
		String catAttTypeKey = null;
		String typeKey = null;
		boolean isBuildVacinationDroplist = false;
		boolean alreadyAddedCovidVaccinationCertificate = false;
		for (TypeInfo docTipe : allDocs) {
			typeKey = docTipe.getTypeKey();
			boolean vaccinated = false;
			boolean isScanQRCode = false;
			boolean isDisplayDoc = false;
			boolean isMandatoryDoc = false;
			String onbaseDocTipe = null;
			String vaccinationTypeMap = null;
			//
			catAttTypeKey = "vss.code.COVIDSTATUSAP.QG";
			List<TypeAttributeValueInfo> results = typeService.getTypeAttributeValues(typeKey, catAttTypeKey);
			for (TypeAttributeValueInfo addAttValue : results) {
				if (addAttValue.getTypeAttributeValue().toUpperCase().equals("YES")) {

					// docTipesForVaccinated.put(typeKey, docTipe);
					vaccinated = true;
					
				} else {
					vaccinated = false;
					// docTipesForUnVaccinated.put(typeKey, docTipe);
				}
			}
			//
			catAttTypeKey = "vss.code.COVIDSTATUSAP.1";
			results = typeService.getTypeAttributeValues(typeKey, catAttTypeKey);
			for (TypeAttributeValueInfo addAttValue : results) {
				if (addAttValue.getTypeAttributeValue().toUpperCase().equals("YES")) {
					isMandatoryDoc = true;
				} else {
					isMandatoryDoc = false;
				}
			}
			//
			catAttTypeKey = "vss.code.COVIDSTATUSAP.QRCDE";
			results = typeService.getTypeAttributeValues(typeKey, catAttTypeKey);
			for (TypeAttributeValueInfo addAttValue : results) {
				if (addAttValue.getTypeAttributeValue().toUpperCase().equals("YES")) {
					isScanQRCode = true;
				} else {
					isScanQRCode = false;
				}
			}
			//			//
			catAttTypeKey = "vss.code.COVIDSTATUSAP.DISPL";
			results = typeService.getTypeAttributeValues(typeKey, catAttTypeKey);
			for (TypeAttributeValueInfo addAttValue : results) {
				if (addAttValue.getTypeAttributeValue().toUpperCase().equals("YES")) {
					isDisplayDoc = true;
					
					
				} else {
					isDisplayDoc = false;
				}
			}
			//
			catAttTypeKey = "vss.code.COVIDSTATUSAP.DOC";
			results = typeService.getTypeAttributeValues(typeKey, catAttTypeKey);
			for (TypeAttributeValueInfo addAttValue : results) {
				onbaseDocTipe = addAttValue.getTypeAttributeValue().toUpperCase();
			}
			//
			catAttTypeKey = "vss.code.COVIDSTATUSAP.WSNME";
			results = typeService.getTypeAttributeValues(typeKey, catAttTypeKey);
			if (results != null) {
				for (TypeAttributeValueInfo addAttValue : results) {
					vaccinationTypeMap = addAttValue.getTypeAttributeValue().toUpperCase();			
					vaccinationTypeInfoMap.put(vaccinationTypeMap,docTipe);
				}

			}


			//
			UploadDTO fileUploadDTO = new UploadDTO(vaccinated, isDisplayDoc, isMandatoryDoc, isScanQRCode , onbaseDocTipe, vaccinationTypeMap);


			if (vaccinated) {
					if (!alreadyAddedCovidVaccinationCertificate) {
//						docTipesForVaccinated.put(typeKey, fileUploadDTO);
						docTipesForVaccinated.put(fileUploadDTO.onbaseDocTipe,fileUploadDTO);
						// Only add doctype once.
						if (fileUploadDTO.onbaseDocTipe.toUpperCase().contains("COVID VACCINATION CERTIFICATE")) {
							isBuildVacinationDroplist = true;
							alreadyAddedCovidVaccinationCertificate = true;
						}
					}
					if (isDisplayDoc) {	// This list will be used to populate the vaccination droplist
						vaccinationTypeList.add(docTipe);
					}
			} else {
//					docTipesForUnVaccinated.put(typeKey, fileUploadDTO);
					docTipesForUnVaccinated.put(fileUploadDTO.onbaseDocTipe, fileUploadDTO);
			}
		}

		// Filter out the duplicate document types...

		// Then build the doc up load for the seleted Y or N
		System.out.println("Vaccinated ?  " + vacStatus);
		resetUploadComponents();

		if (vacStatus.equals("YES")) {

			for (Entry<String, UploadDTO> entry : docTipesForVaccinated.entrySet()) {
			    UploadDTO docTipe = entry.getValue();
			    buildDocsToUploadPanel(uploadVL, docTipe);
			}
		} else {
			for (Entry<String, UploadDTO> entry : docTipesForUnVaccinated.entrySet()) {
			    UploadDTO docTipe = entry.getValue();
			    buildDocsToUploadPanel(uploadVL, docTipe);
			}

		}
		doUploadLayout.setVisible(true);

		getUploadComponentLayout().addComponent(uploadVL);
		getUploadComponentLayout().setVisible(true);
		getUploadLayout().setVisible(true);
		
		getUploadInstructionsLayout().setVisible(true);
		
		if (isBuildVacinationDroplist) {
			vacTypeComboBox = setupVaccinationDroplist(vaccinationTypeList);
			getUploadLayout().addComponent(vacTypeComboBox);
		}
	}

	private void resetUploadComponents() {
		// REset layout and the component map and rebuild it.

		if (uploadVL != null) {
			uploadVL.removeAllComponents();
		} else {
			uploadVL = new VerticalLayout();
		}
		uploadComponentMap = new HashMap<String, UploadComponent>();
	}

	private void buildDocsToUploadPanel(VerticalLayout uploadVL, UploadDTO docTipe) {
		// TODO BUILD die panel met 'n label en file upload vir elke document tipe
		// TODO NINA use uplaodDTO to set certain things...
		System.out.println("Doc Type : " + docTipe.onbaseDocTipe );
		
		if (docTipe.isDisplayDoc) {		// Only build type if it needs to be displayed

			HorizontalLayout docTypeHL = new HorizontalLayout();
			Label docLabel = new Label(docTipe.onbaseDocTipe);
			UploadComponent docTypeUpload = new UploadComponent();

			String onbaseDocTypeKey = "ONBASE.CONFIG.DOCUMENTYPE." + docTipe.onbaseDocTipe;
			docTypeUpload.init(eventBus, vaadinI18n, documentManagementServiceProxy, executionContext.getLookupUser(),
					systemLanguage, onbaseDocTypeKey, false, true);
			docTypeUpload.setUniversityNumber(executionContext.getLookupUser());
			docTypeUpload.setVisible(true);

			if (docTipe.isMandatoryDoc) {
				// TODO Henriko -  hoe kan ek die UploadComponent required maak dat die * wys?
				// docTypeUpload.
			}
			docTypeHL.addComponent(docLabel);
			docTypeHL.addComponent(docTypeUpload);
			if (!docTypeHL.isAttached()) {
				uploadVL.addComponent(docTypeHL);
				uploadVL.setVisible(true);
			}

			if (uploadComponentMap == null) {
				uploadComponentMap = new HashMap<String, UploadComponent>();
			}
			uploadComponentMap.put(docTipe.onbaseDocTipe, docTypeUpload);
			
		}
	}

	private void populateVaccinationDocumentHistoryTable() {
		vaccinationDocumentHistoryGrid.removeAllColumns();
		setGridItems();

		vaccinationDocumentHistoryGrid.setWidth(100, Unit.PERCENTAGE);
		vaccinationDocumentHistoryGrid.setHeightMode(HeightMode.UNDEFINED);
		vaccinationDocumentHistoryGrid.setItems(vaccinationDocumentHistoryList);
		vaccinationDocumentHistoryGrid.addComponentColumn(this::documentType)
				.setCaption(vaadinI18n.getMessage("svs.vaccination.table.document.type"));
		vaccinationDocumentHistoryGrid.addComponentColumn(this::downloadLink)
				.setCaption(vaadinI18n.getMessage("svs.vaccination.table.download"));
		vaccinationDocumentHistoryGrid.addComponentColumn(this::createdBy)
				.setCaption(vaadinI18n.getMessage("svs.vaccination.table.created.by"));
		vaccinationDocumentHistoryGrid.addComponentColumn(this::created)
				.setCaption(vaadinI18n.getMessage("svs.vaccination.table.created"));

		if (vaccinationDocumentHistoryList.isEmpty()) {
			addEmptyRow();
		}

		vaccinationDocumentHistoryLayout.addComponent(vaccinationDocumentHistoryGrid);
	}

	private HorizontalLayout documentType(VaccinationDocumentHistory record) {
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		if (record.getDocumentType() != null) {
			Label label = new Label(record.getDocumentType());
			horizontalLayout.addComponents(label);
		}
		return horizontalLayout;
	}

	private HorizontalLayout downloadLink(VaccinationDocumentHistory record) {
		HorizontalLayout horizontalLayout = new HorizontalLayout();

		if(record.getDocumentTypeKey() == null) {
			return horizontalLayout;
		}

		OnbaseFileLinkButtonForGrid onbaseFileLinkButton = new OnbaseFileLinkButtonForGrid(record.getDocumentDisplayname(),
				record.getDocumentFileType(), record.getDocumentId(), documentManagementServiceProxy);

		horizontalLayout.addComponents(onbaseFileLinkButton);

		return horizontalLayout;
	}

	private HorizontalLayout createdBy(VaccinationDocumentHistory record) {
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		if (record.getCreatedBy() != null) {
			Label label = new Label(record.getCreatedBy());
			horizontalLayout.addComponents(label);
		}
		return horizontalLayout;
	}

	private HorizontalLayout created(VaccinationDocumentHistory record) {
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		if (record.getCreated() != null) {
			Label label = new Label(DateUtils.shortDateFormat(record.getCreated()));
			horizontalLayout.addComponents(label);
		}
		return horizontalLayout;
	}

	private void setGridItems() {
		vaccinationDocumentHistoryList = new ArrayList<>();

		HashMap<String, String> documentKeywords = new HashMap<>();
		documentKeywords.put(StudentCovidStatusConstants.ONBASE_UNIVERSITY_KEYWORD, executionContext.getLookupUser());
		List<DocumentInfo> uploadedDocs = documentManagementServiceProxy.getDocumentsBySearchCriteria(
				null, documentKeywords, 500, "vss.code.LANGUAGE.3");

		if (uploadedDocs != null && uploadedDocs.size() >= 1) {
			for (DocumentInfo documentInfo : uploadedDocs) {

				VaccinationDocumentHistory history = new VaccinationDocumentHistory();

				if (documentInfo.getDocumentKeywords() != null) {
					if (!Strings.isNullOrEmpty(documentInfo.getDocumentKeywords()
							.get("ONBASE.CONFIG.KEYWORD.University Number.University Number"))) {
						history.setCreatedBy(documentInfo.getDocumentKeywords()
								.get("ONBASE.CONFIG.KEYWORD.University Number.University Number"));
					} else {
						history.setCreatedBy("");
					}
				} else {
					history.setCreatedBy("");
				}

				if (documentInfo.getMetaInfo() != null) {
					if (documentInfo.getMetaInfo().getCreateTime() != null) {
						history.setCreated(documentInfo.getMetaInfo().getCreateTime());
					} else {
						history.setCreated(null);
					}
				} else {
					history.setCreated(null);
				}

				if (!Strings.isNullOrEmpty(documentInfo.getDocumentTypeTypeKey())) {
					history.setDocumentType(UIUtils.splitOnVssKey(documentInfo.getDocumentTypeTypeKey(), 4));
				} else {
					history.setDocumentType("");
				}

				history.setDocumentId(documentInfo.getDocumentID());
				history.setDocumentTypeKey(documentInfo.getDocumentTypeTypeKey());
				history.setDocumentDisplayname(documentInfo.getDocumentDisplayName());
				history.setDocumentFileType(documentInfo.getDefaultFileTypeTypeKey());

				vaccinationDocumentHistoryList.add(history);
			}
		}
	}

	private void addEmptyRow() {
		vaccinationDocumentHistoryList.add(new VaccinationDocumentHistory());
	}

	protected void addDisclaimers(Boolean disclaimerRequired) {
		if (disclaimerRequired) {
			vacdisclaimerLayout.removeAllComponents();
			vacdisclaimerLayout.setVisible(true);
			// TODO NINA Add new Vacination disclaimers here.
			// vacdisclaimerLayout.addComponents(studentDisclaimer);
		} else {
			vacdisclaimerLayout.setVisible(false);
		}
	}

	@Subscribe
	public void onUpload(UploadEvent uploadEvent) {
		// These are actual file that was selected for upload. So
		// filesThatNeedsToBeUploadedToOnbase is a map of docs that was selected by the
		// user that neesd to be uploaded
		// Meaning that if there are 10 options and 3 are selected, that only those 3
		// will appear in the map

		// Get related uploadComponent to clear errors
		UploadComponent comp = uploadComponentMap.get(uploadEvent.getDocumentTypeKey());
		if (comp != null) {
			comp.setComponentError(null);
		}

		if (filesThatNeedsToBeUploadedToOnbase == null) {
			filesThatNeedsToBeUploadedToOnbase = new HashMap<String, OnbaseContainer>();
		}
		filesThatNeedsToBeUploadedToOnbase.put(uploadEvent.getDocumentTypeKey(), uploadEvent.getOnbaseContainer());
		if (docTipesForVaccinated != null) {
			UploadDTO docDetails = docTipesForVaccinated.get(UIUtils.splitOnVssKey(uploadEvent.getDocumentTypeKey(), 3));
			try {
				if (docDetails != null) {
				    validateQRCodeOnDocTipes(docDetails);					
				}
				
			} catch (OperationFailedException e) {
				TypeInfo selectedVac = vacTypeComboBox.getValue();
				if (selectedVac == null) {
					vacTypeComboBox.setVisible(true);
					vacTypeComboBox.setRequiredIndicatorVisible(true);

					UI.getCurrent().access(new Runnable() {
						@Override
						public void run() {
							throw new VaadinUIException("qr.validation.failed.pls.complete.vaccinationtype", true);
						}
					});
				}
			}
		}
	}

	public void eventToClearError(TextField currentTextField) {
		currentTextField.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
			@Override
			public void valueChange(HasValue.ValueChangeEvent<String> valueChangeEvent) {
				currentTextField.setComponentError(null);
				eventBus.post(new ClearErrorEvent());
			}
		});
	}

	public void eventToClearError(TextArea currentTextArea) {
		currentTextArea.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
			@Override
			public void valueChange(HasValue.ValueChangeEvent<String> valueChangeEvent) {
				currentTextArea.setComponentError(null);
				eventBus.post(new ClearErrorEvent());
			}
		});
	}

	public void eventToClearError(ComboBox currentComboBox) {
		currentComboBox.addValueChangeListener(new HasValue.ValueChangeListener<String>() {
			@Override
			public void valueChange(HasValue.ValueChangeEvent<String> valueChangeEvent) {
				currentComboBox.setComponentError(null);
				eventBus.post(new ClearErrorEvent());
			}
		});
	}

//	public void uploadDocument() {
//		// TODO NINA this must be fixed.
//		if (covidVacCertOnbaseContainer != null) {
//
//			documentManagementCRUDServiceProxy.createDocument(covidVacCertOnbaseContainer.getOnbaseDocument(),
//					covidVacCertOnbaseContainer.getActualByteArrayToBeUploaded(), systemLanguage);
//		}
//	}

	public void setUniversityNumberTextfieldVisible(Boolean visible) {
		lblUnivNumber.setVisible(!visible);
		txtUnivNum.setVisible(visible);
	}

	private void loadPersonDetails() {
		try {
			personBiographic = personProxyService.getPersonBiographicByLang(executionContext.getLookupUser(),
					executionContext.getSystemLanguageTypeKey());

		} catch (Exception e) {
			log.error("Could not load personal biographic info: " + e.getMessage(), e);
			throw new VaadinUIException("error.person.service.biographic", true);
		}
	}

	@Translate
	private void displaySuccessfullMsg() {
		Label popupMessage = new Label(vaadinI18n.getMessage("svs.save.success.popup.message"));
		popupMessage.setContentMode(ContentMode.HTML);
		popupMessage.setSizeFull();
		Button button = new Button(vaadinI18n.getMessage("svs.save.success.popup.button.ok"));
		Window window = new Window(vaadinI18n.getMessage("svs.save.success.popup.title"));
		window.setResizable(false);
		window.center();
		window.setWidth(300.0f, Unit.PIXELS);
		VerticalLayout popupContent = new VerticalLayout();
		popupContent.setMargin(true);
		popupContent.addComponents(popupMessage, button);
		popupContent.setComponentAlignment(button, Alignment.MIDDLE_RIGHT);
		window.setContent(popupContent);
		UI.getCurrent().addWindow(window);

		button.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent clickEvent) {
				window.close();
				navigateToDIY();
			}
		});

		window.addCloseListener(new Window.CloseListener() {
			@Override
			public void windowClose(Window.CloseEvent closeEvent) {
				window.close();
				navigateToDIY();
			}
		});
	}

	private void navigateToDIY() {
		if (UI.getCurrent().getLocale().getLanguage().equals("af")) {
			getUI().getPage().setLocation(appConfig.getDIY_UrlAF());
		} else {
			getUI().getPage().setLocation(appConfig.getDIY_UrlEN());
		}
	}

	public void eventListeners() {
		isVacStatusRadioButton.setRequiredIndicatorVisible(true);
		lblUnivNumber.setValue(executionContext.getLookupUser());
		txtUnivNum.setRequiredIndicatorVisible(true);
		txtUnivNum.setValueChangeMode(ValueChangeMode.BLUR);

		txtUnivNum.addValueChangeListener((HasValue.ValueChangeListener<String>) event -> {
			txtUnivNum.setComponentError(null);
			executionContext.setLookupUser(txtUnivNum.getValue());
			executionContext.setAuthenticatedUser(txtUnivNum.getValue());
			boolean doLayoutVisibility = false;
			clearUserData();
			loadInitialData(doLayoutVisibility);
		});

		getDisclaimerChkBox().addValueChangeListener(new HasValue.ValueChangeListener<Boolean>() {
			@Override
			public void valueChange(HasValue.ValueChangeEvent<Boolean> event) {
				if (event.getValue()) {
					toggleLayouts(true);
					getDisclaimerChkBox().setVisible(false);
					getDisclaimerHead().setVisible(false);
				} else {
					toggleLayouts(false);
				}
			}
		});

		btnSubmit.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				resetErrorMsgOnAllInputComponents();
				submitValidations();
				save();
			}

			private void resetErrorMsgOnAllInputComponents() {
				isVacStatusRadioButton.setComponentError(null);
//				covidVacCertUploadComponent.setComponentError(null);
				vaccinationDocumentHistoryGrid.setComponentError(null);
				StudentCovidStatusCrudUI.getCurrent().clearErrors();
			}
		});

		isVacStatusRadioButton.addValueChangeListener((HasValue.ValueChangeListener<String>) event -> {
			eventBus.post(new ClearErrorEvent());
			if (event.getValue() != null) {
				if (event.getValue().equals("No") || event.getValue().equals("Nee")) {
//                doUploadLayout.setVisible(false);
					if(vacTypeComboBox != null){
						vacTypeComboBox.setVisible(false);
						StudentCovidStatusCrudUI.getCurrent().clearErrors();
					}
					isVaccinatedAgainstCovid = false;
					documentTypesByVacStatus("NO");

				} else if (event.getValue().equals("Yes") || event.getValue().equals("Ja")) {
//                doUploadLayout.setVisible(false);
					isVaccinatedAgainstCovid = true;
					documentTypesByVacStatus("YES");
				}

			}
		});
	}

	private void validlateUploadedDocs() {
		if (isVaccinatedAgainstCovid) {
			for (Entry<String, UploadDTO> entry : docTipesForVaccinated.entrySet()) {
			    validateRequiredDocTipes(entry);
			    try {
				    validateQRCodeOnDocTipes(entry.getValue());
					
				} catch (OperationFailedException e) {
					TypeInfo selectedVac = vacTypeComboBox.getValue();
					if (selectedVac == null) {
						vacTypeComboBox.setVisible(true);
						vacTypeComboBox.setRequiredIndicatorVisible(true);
						throw new VaadinUIException("qr.validation.failed.pls.complete.vaccinationtype", true);
					}
				}
			}
		} else {
			for (Entry<String, UploadDTO> entry : docTipesForUnVaccinated.entrySet()) {
			    validateRequiredDocTipes(entry);
			}
		}
			

	}


	private ComboBox<TypeInfo> setupVaccinationDroplist(List<TypeInfo> vaccinationTypeList) {
		ComboBox<TypeInfo> vaccinationTypeComboBox;
//		List<TypeInfo> vaccinationList;
		
//		vaccinationList = typeService.getTypesByCategory("vss.code.COVIDSTATUSAP", executionContext.getSystemLanguageTypeKey());
		vaccinationTypeComboBox = new ComboBox<>(vaadinI18n.getMessage("ui.label.list.of.vaccinations"));


		vaccinationTypeComboBox.setItems(vaccinationTypeList);
	
		vaccinationTypeComboBox.setItemCaptionGenerator(new ItemCaptionGenerator<TypeInfo>() {
            @Override
            public String apply(TypeInfo item) {
            	
                return UIUtils.splitOnVssKey(item.getTypeKey(), 4);
            }
        });
		
		
		vaccinationTypeComboBox.setRequiredIndicatorVisible(true);
		vaccinationTypeComboBox.setEmptySelectionAllowed(true);
		vaccinationTypeComboBox.setTextInputAllowed(true);
		vaccinationTypeComboBox.setPageLength(10);

		vaccinationTypeComboBox.setVisible(false);
		return vaccinationTypeComboBox;
	}

	private void validateRequiredDocTipes(Entry<String, UploadDTO> entry) {
		UploadDTO docTipe = entry.getValue();
		if (docTipe.isMandatoryDoc) {
			if (filesThatNeedsToBeUploadedToOnbase == null) {
				throw new VaadinUIException("svs.upload.dc.validation", true);
			}
			OnbaseContainer fileWasUploaded = filesThatNeedsToBeUploadedToOnbase.get("ONBASE.CONFIG.DOCUMENTYPE." + docTipe.onbaseDocTipe);
			if (fileWasUploaded == null) {
				throw new VaadinUIException("svs.upload.dc.validation", true);
			} else {
				if (fileWasUploaded.getActualByteArrayToBeUploaded() == null) {
					throw new VaadinUIException("svs.upload.dc.validation", true);
				}
			}
		}
	}
	
	private void validateQRCodeOnDocTipes(UploadDTO docTipe) throws OperationFailedException {
//		String key = entry.getKey();
//		UploadDTO docTipe = entry.getValue();
		if (docTipe.isScanQRCode) {
			if (filesThatNeedsToBeUploadedToOnbase != null) {
				OnbaseContainer fileWasUploaded = filesThatNeedsToBeUploadedToOnbase.get(docTipe.onbaseDocTipe);
				// TODO The service call must still be implemented for now lets thru OperationFailedExcpetion so that
				// the rest of the implementation can be written.
				throw new OperationFailedException("QRCode integration not yet implemented");
			}
		}
	}

	private void submitValidations() {
		validateRequiredInputFieds();
		validlateUploadedDocs();
	}

	private void validateRequiredInputFieds() {
		if (isVacStatusRadioButton.getValue() == null) {
			throw new VaadinUIException("svs.vac.status.required", true);
		}
		if (vacTypeComboBox.isVisible() && vacTypeComboBox.getValue() == null) {
			throw new VaadinUIException("svs.vac.type.required", true);			
		}

	}

	private void save() {
		mapDataFromUIForSave();
		saveDocuments();
		saveCovidStatus();
		displaySuccessfullMsg();

	}

	private PersonVaccinactionInfo mapDataFromUIForSave() {
		vacInfo = new PersonVaccinactionInfo();
		MetaInfo meta = new MetaInfo();
		meta.setCreateId(executionContext.getAuthenticatedUser());
		meta.setAuditFunction("999"); // TODO Gert wat moet die Audit Funksie wees?
		vacInfo.setMetaInfo(meta);

		vacInfo.setUnivNumber(executionContext.getLookupUser());
		if (isVaccinatedAgainstCovid) {
			vacInfo.setVacStatusTypeKey(
					PersonVaccinactionInfo.PERSON_VAC_STATUS_TYPEKEY + PERSON_VAC_STATUS_FULLYVACINATED);
		} else {
			vacInfo.setVacStatusTypeKey(
					PersonVaccinactionInfo.PERSON_VAC_STATUS_TYPEKEY + PERSON_VAC_STATUS_UNVACINATED);
			
			//
			if (filesThatNeedsToBeUploadedToOnbase != null) {	
			
			for (Entry<String, OnbaseContainer> entry : filesThatNeedsToBeUploadedToOnbase.entrySet()) {
			    String key = entry.getKey();
			    OnbaseContainer value = entry.getValue();
			    // ...
			    if (key.contains("COVID VACCINATION EXEMPTION")) {
					vacInfo.setExcemptionStatusTypeKey(PersonVaccinactionInfo.PERSON_EXCEMPTSTATUS_TYPEKEY + "." + "1");					
				}
			    if (key.contains("NEGATIVE COVID TEST")) {
			    	vacInfo.setCovidTestResultTypeKey(PersonVaccinactionInfo.PERSON_COVIDTEST_RESULT_TYPEKEY + "." + "2");
				}
			}
			}

		}

		if (vacTypeComboBox != null) {
			TypeInfo selectedVacTypeKey = vacTypeComboBox.getValue();
			if (selectedVacTypeKey != null) {
				vacInfo.setVacTypeKey(selectedVacTypeKey.getTypeKey());
				
			}			
		} else {
			// Leave blank vacInfo.setVacTypeKey(PersonVaccinactionInfo.PERSON_VACCINATION_TYPEKEY + PERSON_VACCINATION_COVID);			
		}

		vacInfo.setCaptureDate(GregorianCalendar.getInstance().getTime());

		return vacInfo;

	}

	private Date getCurrentDate() {
		Date tempDate = GregorianCalendar.getInstance().getTime();
		String myString = DateUtils.shortDateFormat(tempDate);
		return DateUtils.convertToDate(DateUtils.parseShortDate(myString));
	}

	private void saveCovidStatus() {
		try {
			PersonVaccinactionInfo insertedVacInfo = personProxyService.insertVacStatus(vacInfo);
		} catch (DoesNotExistException | InvalidParameterException | MissingParameterException
				| OperationFailedException | PermissionDeniedException e) {
			log.error("personProxyService.insertVacStatus : " + e.getMessage(), e);
			throw new VaadinUIException(e.getMessage());
		}

	}

	private void saveDocuments() {
		try {

			if (filesThatNeedsToBeUploadedToOnbase != null) {
				for (Entry<String, OnbaseContainer> entry : filesThatNeedsToBeUploadedToOnbase.entrySet()) {
					System.out.println("Uploading doc Type : " + entry.getKey());
					if (entry.getValue() != null) {
						OnbaseContainer docToUpload = (OnbaseContainer) entry.getValue();
						String docTypeKey = docToUpload.getOnbaseDocument().getDocumentTypeTypeKey();
						if (docTypeKey.contains("COVID VACCINATION CERTIFICATE")) {
							HashMap<String, String> keywords = docToUpload.getOnbaseDocument().getDocumentKeywords();
							TypeInfo selectedVacTypeKey = vacTypeComboBox.getValue();
							keywords.put(StudentCovidStatusConstants.ONBASE_VACCINATION_TYPE_KEYWORD, UIUtils.splitOnVssKey(selectedVacTypeKey.getTypeKey(), 3));
							docToUpload.getOnbaseDocument().setDocumentKeywords(keywords);
						}
						documentManagementCRUDServiceProxy.createDocument(docToUpload.getOnbaseDocument(),
								docToUpload.getActualByteArrayToBeUploaded(), systemLanguage);
					}
				}
			}
		} catch (Exception e) {
			log.error("uploadDocument failed");
			log.error("uploadDocument failed: " + e.getMessage(), e);
			throw new VaadinUIException(e.getMessage());
		}

	}

	private void resetScreenVisibleComponents() {
		// Defualt state for this button
		this.getBtnSubmit().setEnabled(true);
		this.getIsVacStatusRadioButton().setEnabled(true);
		//
		isVacStatusLayout.setVisible(true);
		doUploadLayout.setVisible(false);
	}

	public void setRadioButtonsTranslation() {
		List<String> translatedYesAndNo = new ArrayList<>();
		translatedYesAndNo.add(vaadinI18n.getMessage("svs.component.yes"));
		translatedYesAndNo.add(vaadinI18n.getMessage("svs.component.no"));

		getIsVacStatusRadioButton().setItems(translatedYesAndNo);
	}

	private void loadInitialData(boolean doLayoutVisibility) {
		if (doLayoutVisibility) {
			toggleLayouts(false);
		}
		setRadioButtonsTranslation();
		loadPersonDetails();
		mapDataTOUI();
//		setupCovidVacCertificateUploadComponent();
		populateVaccinationDocumentHistoryTable();
	}

	public void clearUserData() {
		getIsVacStatusRadioButton().setValue(null);

//		if (covidVacCertOnbaseContainer != null) {
//			covidVacCertUploadComponent.getOnbaseContainer().setActualByteArrayToBeUploaded(null);
//		}
	}

    private void toggleLayouts(Boolean visible) {
        isVacStatusLayout.setVisible(visible);
//        doUploadLayout.setVisible(visible);
        vacdisclaimerLayout.setVisible(visible);
        vaccinationDocumentHistoryLayout.setVisible(visible);
        buttonVeritcalLayout.setVisible(visible);
        lblInstructional.setVisible(visible);
    }

	private void mapDataTOUI() {
		String title = UIUtils.convertVssTypeKeyToName(personBiographic.getTitleTypeKey());
		this.txtUnivNum.setValue(personBiographic.getUnivNumber());
		this.lblTitleInitialsSurname
				.setValue(title + " " + personBiographic.getInitials() + " " + personBiographic.getLastName());
	}

	@Translate("svd.component.upload.disclaimer")
	public Label getLblUploadInstructions() {
		return LblUploadInstructions;
	}

	@Translate("svs.disclaimer.agree")
	public CheckBox getDisclaimerChkBox() {
		return disclaimerChkBox;
	}

	public HorizontalLayout getUploadLayout() {
		return uploadLayout;
	}
    @Translate("svs.instructional")
    public Label getLblInstructional() {
        return lblInstructional;
    }

	@Translate("svs.component.header.caption")
	public Label getHeader() {
		return labelHeader;
	}

	@Translate("svs.component.lblDisclaimerHead.caption")
	public Label getDisclaimerHead() {
		return lblDisclaimerHead;
	}

	@Translate("svs.component.universityNumberLabel.caption")
	public Label getUniversityNumberLable() {
		return lblUnivNum;
	}

	@Translate("svs.component.LblIsVacStatus.caption")
	public Label getLblIsVacStatus() {
		return LblIsVacStatus;
	}

	public RadioButton isRequirePrivateAccommodation() {
		return isRequirePrivateAccommodation();
	}

	@Translate("svs.component.btnSubmit.caption")
	public Button getBtnSubmit() {
		return btnSubmit;
	}

	public HorizontalLayout getUploadComponentLayout() {
		return uploadComponentLayout;
	}

	public RadioButtonGroup<java.lang.String> getIsVacStatusRadioButton() {
		return isVacStatusRadioButton;
	}

	public void setIsVacStatusRadioButton(RadioButtonGroup<java.lang.String> isVacStatusRadioButton) {
		this.isVacStatusRadioButton = isVacStatusRadioButton;
	}
	
	public HorizontalLayout getUploadInstructionsLayout() {
		return this.uploadInstructionsLayout;
	}
}
