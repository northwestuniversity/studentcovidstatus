package nwu.student.ui.components;

import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.vaadin.server.Page;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.ui.utils.DateUtils;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.document.dto.DocumentInfo;
import nwu.student.StudentCovidStatusCrudUI;
import nwu.student.events.UploadEvent;
import nwu.student.ui.dto.OnbaseContainer;
import nwu.student.ui.dto.OnbaseFileLinkButton;
import nwu.student.ui.service.DocumentManagementServiceProxy;
import nwu.student.ui.util.StudentCovidStatusConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class UploadComponent extends CustomComponent {
    private static final long serialVersionUID = 1L;
    public static final String PDF_FILE_TYPE = "ONBASE.CONFIG.FILETYPE.PDF";
    public static final String DOCUMENT_TYPE_BASE = "ONBASE.CONFIG.DOCUMENTTYPE.";

    private byte[] uploadedFileByteArray = new byte[1024];
    private String actualFileName = new String("");
    private String fileExtension = new String("");
    private OnbaseContainer onbaseContainer;
    private String fileUploadButtonLabel;

    private VaadinI18n vaadinI18n;
    private EventBus eventBus;
    private DocumentManagementServiceProxy documentManagementServiceProxy;
    private OnbaseFileLinkButton onbaseFileLinkButton;
    private String lookupUser;

    private final Logger log = LoggerFactory.getLogger(UploadComponent.class.getName());
    private Upload upload;

    private boolean unsupportedFileType = false;

    public void init(EventBus eventBus, VaadinI18n vaadinI18n,
                     DocumentManagementServiceProxy documentManagementServiceProxy, String lookupUser, String systemLanguage,
                     String documentType, Boolean loadDocumentFromDB, Boolean uploadVisible) {
        this.setComponentError(null);
        this.eventBus = eventBus;
        this.vaadinI18n = vaadinI18n;
        this.documentManagementServiceProxy = documentManagementServiceProxy;
        this.lookupUser = lookupUser;
        VerticalLayout layout = new VerticalLayout();
        FileReceiver receiver = new FileReceiver();

        // Create the upload with a caption and set receiver later
        upload = new Upload(null, receiver);
        upload.setButtonCaption("");
        upload.addSucceededListener(receiver);
        upload.addFailedListener(receiver);
        upload.addFinishedListener(receiver);
        upload.setVisible(uploadVisible);

        Calendar calendar = Calendar.getInstance();

        HashMap<String, String> keywords = new HashMap<>();
        keywords.put(StudentCovidStatusConstants.ONBASE_UNIVERSITY_KEYWORD, lookupUser);
        // TODO NINA replace with authenticated user
        keywords.put(StudentCovidStatusConstants.ONBASE_UPLOADED_BY_UNIVERSITY_KEYWORD,lookupUser);

        DocumentInfo documentInfo = new DocumentInfo();
        documentInfo.setCreatedDate(DateUtils.shortDateFormat(new Date()));
        documentInfo.setDefaultFileTypeTypeKey(PDF_FILE_TYPE);
        documentInfo.setDocumentDisplayName("");
        documentInfo.setDocumentKeywords(keywords);
        documentInfo.setDocumentTypeTypeKey(documentType);

        if(loadDocumentFromDB) {
            try {
                documentInfo = documentManagementServiceProxy.getLatestDocumentByDocumentTypeAndKeyword(
                        documentType, keywords, 10, systemLanguage);

            } catch (Exception e) {
                // Creating placeholder in the table for the file to be
                // uploaded as it is required
                documentInfo = new DocumentInfo();
                documentInfo.setCreatedDate(DateUtils.shortDateFormat(new Date()));
                documentInfo.setDefaultFileTypeTypeKey(PDF_FILE_TYPE);
                documentInfo.setDocumentTypeTypeKey(documentType);
                documentInfo.setDocumentDisplayName("");
                documentInfo.setDocumentKeywords(keywords);

                // }
                log.warn("Something went wrong with retrieving Onbase Documents but showing required to user: " +
                        e.getMessage(), e);
            }
        }

        if(onbaseContainer == null) {
            onbaseContainer = new OnbaseContainer();
        }

        onbaseContainer.setOnbaseDocument(documentInfo);

        onbaseFileLinkButton = new OnbaseFileLinkButton(onbaseContainer, vaadinI18n,
                documentManagementServiceProxy, eventBus);
        HorizontalLayout uploadLayout = new HorizontalLayout(upload, onbaseFileLinkButton);

        layout.setSpacing(false);
        layout.setMargin(false);
        layout.setCaption(null);
        layout.addComponent(uploadLayout);
        setCaption(null);

        setCompositionRoot(layout);
    }

    public void setUniversityNumber(String lookupUser) {

        if(onbaseContainer != null) {
            if(onbaseContainer.getOnbaseDocument().getDocumentKeywords() != null) {

                HashMap<String, String> keywords = onbaseContainer.getOnbaseDocument().getDocumentKeywords();
                keywords.replace(StudentCovidStatusConstants.ONBASE_UNIVERSITY_KEYWORD, lookupUser);

                onbaseContainer.getOnbaseDocument().setDocumentKeywords(keywords);
            }
        }
    }

    public class FileReceiver implements Upload.Receiver, Upload.SucceededListener, Upload.FailedListener, Upload.FinishedListener {
        private static final long serialVersionUID = 1L;
        public File file;

        @Override
        public OutputStream receiveUpload(String filename, String MIMEType) {
            unsupportedFileType = false;
            setActualFileName(filename);

            boolean pdfDocument = MIMEType.equals("application/pdf") && filename.toLowerCase().endsWith(".pdf");
            boolean pngDocument = MIMEType.equals("image/png") && filename.toLowerCase().endsWith(".png");

            boolean jPEGDocument = MIMEType.equals("image/jpeg") &&
                    (filename.toLowerCase().endsWith(".jpeg") | filename.toLowerCase().endsWith(".jpg"));

            // TODO HENRIKO Die files word met verkeerder extention opgelaai ie PNF word as PDF opgelaai en veroorsaak probleme
            // Haal dus hierdie tydelik uit om net PDF toe telaat
            if (!pdfDocument && !jPEGDocument && !pngDocument) {
//           	if (!pdfDocument) {	
                unsupportedFileType = true;
                upload.interruptUpload();
            }

            // Create upload stream
            FileOutputStream fos = null; // Stream to write to

            try {
                // Open the file for writing.
                file = File.createTempFile(UUID.randomUUID().toString(), filename);
                fos = new FileOutputStream(file);
                return fos;
            } catch (final java.io.FileNotFoundException e) {
                e.printStackTrace();
                new Notification("Could not open file<br/>", e.getMessage(), Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
            } catch (IOException ee) {
                new Notification("IO Exception ", ee.getMessage(), Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                ee.printStackTrace();
            }
            return null;
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            String[] parts = event.getFilename().split("\\.");
            fileExtension = parts[parts.length - 1].toLowerCase();
            if(onbaseContainer == null) {
                onbaseContainer = new OnbaseContainer();
            }
            String actualFileNameWithOutExtention = parts[0];
			onbaseContainer.setActualFileName(actualFileNameWithOutExtention);

            onbaseContainer.setFileExtension(fileExtension);
            onbaseContainer.setUploadToOnbase(true);
            FileInputStream fis = null;
            org.apache.commons.io.output.ByteArrayOutputStream bos = null;
            try {
                fis = new FileInputStream(file);
                bos = (org.apache.commons.io.output.ByteArrayOutputStream) convertToOutputStream(fis);
                System.out.println(bos.size());
                uploadedFileByteArray = bos.toByteArray();
                onbaseContainer.setActualByteArrayToBeUploaded(uploadedFileByteArray);

                if(onbaseContainer.getOnbaseDocument() == null) {
                    DocumentInfo documentInfo = new DocumentInfo();
                    documentInfo.setCreatedDate(DateUtils.shortDateFormat(new Date()));
                    documentInfo.setDefaultFileTypeTypeKey(PDF_FILE_TYPE);

                    HashMap<String, String> keywords = new HashMap<>();
                    keywords.put(StudentCovidStatusConstants.ONBASE_UNIVERSITY_KEYWORD, lookupUser);

                    documentInfo.setDocumentKeywords(keywords);
                    onbaseContainer.setOnbaseDocument(documentInfo);
                }

                onbaseContainer.getOnbaseDocument().setDocumentDisplayName(actualFileNameWithOutExtention);
                onbaseContainer.getOnbaseDocument().setDefaultFileTypeTypeKey("ONBASE.CONFIG.FILETYPE." + fileExtension);

                if (onbaseContainer.getOnbaseDocument() != null) {
                    onbaseContainer.getOnbaseDocument().setDocumentDisplayName(onbaseContainer.getOnbaseDocument().getDocumentDisplayName());
                }

                if(onbaseFileLinkButton != null) {

                    onbaseFileLinkButton.setCaption(onbaseContainer.getOnbaseDocument().getDocumentDisplayName()
                            .concat(".").concat(fileExtension));
                }
                eventBus.post(new UploadEvent(onbaseContainer, onbaseContainer.getOnbaseDocument().getDocumentTypeTypeKey()));
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
                throw new RuntimeException("Unable to read file : " + e1.getMessage());

            } finally {
                try {
                    if (fis != null) {
                        fis.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }

                try {
                    if (bos != null) {
                        bos.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }

        @Override
        public void uploadFailed(Upload.FailedEvent event) {
            if(unsupportedFileType) {
                onbaseFileLinkButton.setCaption(getActualFileName());
                onbaseFileLinkButton.setEnabled(false);
                getOnbaseContainer().setActualByteArrayToBeUploaded(null);
                throw new VaadinUIException("svs.error.unsupported.filetype", true);
            }
        }

        @Override
        public void uploadFinished(Upload.FinishedEvent event) {
            upload.setComponentError(null);
            onbaseFileLinkButton.setEnabled(true);
            StudentCovidStatusCrudUI.getCurrent().clearErrors();
        }
    }

    private OutputStream convertToOutputStream(FileInputStream fis) {
        org.apache.commons.io.output.ByteArrayOutputStream bos = new org.apache.commons.io.output.ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum); // no doubt here is 0
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Unable to convert file to byte array for onbase upload");
        }
        return bos;
    }

    public byte[] getUploadedFileByteArray() {
        return uploadedFileByteArray;
    }

    public String getActualFileName() {
        return actualFileName;
    }

    public void setActualFileName(String actualFileName) {
        this.actualFileName = actualFileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public void setUploadButtonLabel(String fileUploadButtonTxt) {
        fileUploadButtonLabel = fileUploadButtonTxt;
    }

    public OnbaseContainer getOnbaseContainer() {
        return onbaseContainer;
    }

    public void setOnbaseContainer(OnbaseContainer onbaseContainer) {
        this.onbaseContainer = onbaseContainer;
    }

    public Upload getUpload() {
        return upload;
    }
}
