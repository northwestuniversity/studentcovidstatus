package nwu.student.ui.dto;

import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.themes.ValoTheme;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.student.ui.service.DocumentManagementServiceProxy;
import nwu.student.ui.util.UIUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class OnbaseFileLinkButton extends Button {

    private static final long serialVersionUID = 1L;
    private final Logger log = LoggerFactory.getLogger(OnbaseFileLinkButton.class.getName());
    private OnbaseContainer onbaseContainer;

    private VaadinI18n vaadinI18n;
    private DocumentManagementServiceProxy documentManagementServiceProxy;
    private EventBus eventBus;

    public OnbaseFileLinkButton(OnbaseContainer onbaseContainer, VaadinI18n vaadinI18n,
                                DocumentManagementServiceProxy documentManagementServiceProxy, EventBus eventBus) {
        super();
        this.onbaseContainer = onbaseContainer;
        this.documentManagementServiceProxy = documentManagementServiceProxy;
        this.eventBus = eventBus;

        String fileName = ""; 	// FOr when there isnot existing doc in Onbase
        if (onbaseContainer.getOnbaseDocument().getDocumentDisplayName() != null) {
            String fileType = onbaseContainer.getOnbaseDocument().getDefaultFileTypeTypeKey();
            String extension = "";

            if(!Strings.isNullOrEmpty(fileType)) {
                extension = fileType.substring(fileType.lastIndexOf(".") + 1);
            }

            // We needed to show the extension.
            if(!Strings.isNullOrEmpty(onbaseContainer.getOnbaseDocument().getDocumentDisplayName())) {
                fileName = onbaseContainer.getOnbaseDocument().getDocumentDisplayName().concat(".").concat(extension);
            }
        }

        this.setCaption(fileName);
        this.setStyleName(ValoTheme.BUTTON_LINK);
        this.addStyleName("v-label-file-upload-file-label");
        this.addStyleName("v-label-file-upload-file-label-success");

        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                if(!getCaption().contains(vaadinI18n.getMessage("document.required"))) {
                    StreamResource streamResource = new StreamResource(
                            new StreamResource.StreamSource() {
                                public InputStream getStream() {
                                    return getInputStreamForDocument();
                                }
                            }, onbaseContainer.getOnbaseDocument().getDocumentDisplayName());
                    streamResource.getStream().setParameter("Content-Disposition", "attachment;filename=" +
                            onbaseContainer.getOnbaseDocument().getDocumentDisplayName());
                    // TODO nina
//                    onbaseContainer.getOnbaseDocument().getDefaultFileTypeTypeKey();
                    String fileMimeType = UIUtils.convertFileTypeToFileMimeType(onbaseContainer.getFileExtension());
                    streamResource.setMIMEType(fileMimeType);
                    streamResource.setCacheTime(0);

                    //Henriko and Nina discussed this. We are going to leave the deprecated code because the problem are with vaadin7 and
                    // will be fixed in later version we are expecting almost reWrite when going to vaadin10 that is why Nina made
                    // the call and said it is okay to leave the deprecated code for now.
                    getUI().getPage().open(streamResource, "_blank", true);
                }
            }

        });
    }

    private InputStream getInputStreamForDocument() {

        if(onbaseContainer.getActualByteArrayToBeUploaded() != null && onbaseContainer.getActualByteArrayToBeUploaded().length > 0) {
            return new ByteArrayInputStream(onbaseContainer.getActualByteArrayToBeUploaded());
        }

        // If it is not found with in the onbaseContainer I am not sure we can make a service call to get it.
        try {
            byte[] documentByte = documentManagementServiceProxy.exportActualDocumentByID(
                    onbaseContainer.getOnbaseDocument().getDocumentID());
            return new ByteArrayInputStream(documentByte);

        } catch (Exception e) {
            log.error("Error while getting document data from the service: " + e.getMessage(), e);
            //We do not want to throw exception if user clicks on this and he still needs to upload.
            return null;
        }
    }
}
