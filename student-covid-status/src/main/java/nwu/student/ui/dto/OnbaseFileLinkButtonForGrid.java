package nwu.student.ui.dto;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.themes.ValoTheme;

import nwu.student.ui.service.DocumentManagementServiceProxy;
import nwu.student.ui.util.UIUtils;

public class OnbaseFileLinkButtonForGrid extends Button {

    private static final long serialVersionUID = 1L;
    private final Logger log = LoggerFactory.getLogger(OnbaseFileLinkButtonForGrid.class.getName());

//    private EventBus eventBus;
    private String onbaseDocumentID;
    private byte[] actualDocumentBytes;
    private DocumentManagementServiceProxy documentManagementServiceProxy;

    public OnbaseFileLinkButtonForGrid(String displayName, String fileType, String onbaseDocID, DocumentManagementServiceProxy docServiceProxy) {
        super();

        this.documentManagementServiceProxy = docServiceProxy;
        this.onbaseDocumentID = onbaseDocID;
//        this.eventBus = eventBus;

        String fileName = ""; 	// FOr when there isnot existing doc in Onbase
        if (displayName != null) {
//            String extension = fileType;

//            if(!Strings.isNullOrEmpty(fileType)) {
//                extension = fileType.substring(fileType.lastIndexOf(".") + 1);
//            }

            // We needed to show the extension.
//            if(!Strings.isNullOrEmpty(displayName)) {
                fileName = displayName;
//            }
        }

        this.setCaption(fileName);
        this.setStyleName(ValoTheme.BUTTON_LINK);
        this.addStyleName("v-label-file-upload-file-label");
        this.addStyleName("v-label-file-upload-file-label-success");

        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
//                if(!getCaption().contains(vaadinI18n.getMessage("document.required"))) {
                    StreamResource streamResource = new StreamResource(
                            new StreamResource.StreamSource() {
                                public InputStream getStream() {
                                    return getInputStreamForDocument();
                                }
                            }, displayName);
                    streamResource.getStream().setParameter("Content-Disposition", "attachment;filename=" +
                    		displayName);
                    
                    String extension = fileType.substring(fileType.lastIndexOf(".") + 1);
                    String fileMimeType = UIUtils.convertFileTypeToFileMimeType(extension);
                    streamResource.setMIMEType(fileMimeType);

                    streamResource.setCacheTime(0);

                    //Henriko and Nina discussed this. We are going to leave the deprecated code because the problem are with vaadin7 and
                    // will be fixed in later version we are expecting almost reWrite when going to vaadin10 that is why Nina made
                    // the call and said it is okay to leave the deprecated code for now.
                    getUI().getPage().open(streamResource, "_blank", true);
//                }
            }
        });
    }

    private InputStream getInputStreamForDocument() {

        if(actualDocumentBytes != null && actualDocumentBytes.length > 0) {
            return new ByteArrayInputStream(actualDocumentBytes);
        }

        // If it is not found with in the onbaseContainer I am not sure we can make a service call to get it.
        try {
            byte[] documentByte = documentManagementServiceProxy.exportActualDocumentByID(onbaseDocumentID);
            actualDocumentBytes = documentByte;
            return new ByteArrayInputStream(documentByte);

        } catch (Exception e) {
            log.error("Error while getting document data from the service: " + e.getMessage(), e);
            //We do not want to throw exception if user clicks on this and he still needs to upload.
            return null;
        }
    }
}
