package nwu.student.ui.dto;

import nwu.enabling.capabilities.supporting.service.document.dto.DocumentInfo;

public class OnbaseContainer {
    private DocumentInfo onbaseDocument;
    private byte[] actualByteArrayToBeUploaded;
    private boolean uploadToOnbase = true;
    private String actualFileName;
    private String fileExtension;

    public DocumentInfo getOnbaseDocument() {
        return onbaseDocument;
    }

    public void setOnbaseDocument(DocumentInfo onbaseDocument) {
        this.onbaseDocument = onbaseDocument;
    }

    public boolean isUploadToOnbase() {
        return uploadToOnbase;
    }

    public void setUploadToOnbase(boolean uploadToOnbase) {
        this.uploadToOnbase = uploadToOnbase;
    }

    public void setActualByteArrayToBeUploaded(byte[] uploadedFileByteArray) {
        this.actualByteArrayToBeUploaded = uploadedFileByteArray;

    }
    public byte[] getActualByteArrayToBeUploaded() {
        return actualByteArrayToBeUploaded;
    }
    public String getActualFileName() {
        return actualFileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setActualFileName(String actualFileName) {
        this.actualFileName = actualFileName;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

}
