package nwu.student.ui.dto;

import java.util.Date;

public class VaccinationDocumentHistory {

	private String createdBy;
	private Date created;
	private String documentType;
	private String documentId;
	private String documentTypeKey;
	private String documentDisplayname;
	private String documentFileType;
	
	

	public String getDocumentFileType() {
		return documentFileType;
	}

	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}

	public String getDocumentDisplayname() {
		return documentDisplayname;
	}

	public void setDocumentDisplayname(String documentDisplayname) {
		this.documentDisplayname = documentDisplayname;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocumentTypeKey() {
		return documentTypeKey;
	}

	public void setDocumentTypeKey(String documentTypeKey) {
		this.documentTypeKey = documentTypeKey;
	}
}
