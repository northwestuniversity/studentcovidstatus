package nwu.student.ui.util;

public class UIUtils {

    public static String convertVssTypeKeyToName(String vssTypeKey) {
        String[] output = vssTypeKey.split("\\.");

        if(output.length > 1) {
            return output[4];
        } else {
            return output[0];
        }
    }

    public static String splitOnVssKey(String value, int length) {
        String[] split = value.split("\\.");
        return split[length];
    }
    
	public static  String convertFileTypeToFileMimeType(String fileType) {
		String fileMimeType = null;
		if (fileType.toUpperCase().contains("PDF")) {
			fileMimeType = "application/pdf";
		}
		if (fileType.toUpperCase().contains("PNG")) {
			fileMimeType = "image/png";
		}
		if (fileType.toUpperCase().contains("JPG") || fileType.toUpperCase().contains("JPEG")) {
			fileMimeType = "image/jpeg";
		}
		return fileMimeType;
	}
}
