package nwu.student.ui.config;

import java.util.Map;

import javax.annotation.PostConstruct;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.student.ui.service.ConfigurationServiceProxy;

@Component
@RefreshScope
public class ApplicationConfig {
	private final Logger log = LoggerFactory.getLogger(ApplicationConfig.class.getName());
	private final String CAS_ENABLED = "CAS.ENABLED";
	private final String CAS_SERVER_URL = "CAS.SERVER.URL";
	private final String ENVIRONMENT_URL = "ENVIRONMENT.URL";
	private final String WEB_CONTEXT = "WEB.CONTEXT";
	private final String DEV_INITIAL_UNIV_NUMBER = "DEV.INITIAL.UNIV.NUMBER";

	public final static String VERSION_GLOBAL = "GLOBAL";

	private final String ALLOW_UNIVERISTY_NUMBER_INPUT = "ALLOW.UNIVERSITY.NUMBER.INPUT";

	private final String Document_Service_User_Group = "DOCUMENT.SERVICE.USER.GROUP";
	public final String Document_Service_User_Password = "DOCUMENT.SERVICE.USER.PASSWORD";

	public final String ADMIN_GROUP = "ADMIN.GROUP";

	private final String DIY_ENG_URL = "DIY.SERVICE.URL";
	private final String DIY_AFR_URL = "DIY.AF.SERVICE.URL";

	@Autowired
	private ConfigurationServiceProxy configServiceProxy;

	@Autowired
	private Config config;

	private Map<String, String> appVersionConfiguration;
	private Map<String, String> appGlobalConfiguration;
	private String configPrefix;
	private String adminGroup;

	private String documentServiceUserGroup;
	private String documentServiceUserPassword;

	@PostConstruct
	public void resetConfiguration() {
		appVersionConfiguration = configServiceProxy.getApllicationVersionSpecificConfiguration();
		appGlobalConfiguration = configServiceProxy.getApplicationGlobalConfiguration();
	}

	public String getCaseEnabled() {
		// If version has cas.enabled property return it, else return global setting
		return getPropertyValue(CAS_ENABLED);
	}

	public String getCASServerUrl() {
		return getPropertyValue(CAS_SERVER_URL);

	}

	public String getEnvironmentUrl() {
		return getPropertyValue(ENVIRONMENT_URL);
	}

	public String getWebContext() {
		return getPropertyValue(WEB_CONTEXT);
	}

	private String getPropertyValue(String propertyKey) {
		if (appVersionConfiguration == null || appGlobalConfiguration == null) {
			resetConfiguration();
		}

		String lookupVersionKey = "/" + getConfigPrefix() + "/V" + config.getAppMajorVersion() + "/" + propertyKey;
		String propertyValue = appVersionConfiguration.get(lookupVersionKey.toUpperCase());
		if (propertyValue != null) {
			return propertyValue;
		} else {
			String lookupGlobalKey = "/" + getConfigPrefix() + "/" + VERSION_GLOBAL + "/" + propertyKey;
			propertyValue = appGlobalConfiguration.get(lookupGlobalKey.toUpperCase());
			return propertyValue;
		}
	}

	public String getConfigPrefix() {
		if (configPrefix != null) {
			return configPrefix;
		}

		configPrefix = config.getRuntimeEnvironment() + "/" + config.getAppName();
		return configPrefix.toUpperCase();
	}

	/**
	 * Only return true if config exist and is equal to true, anything else will
	 * return false
	 */
	public String getDevInitialUnivNumber() {
		String propertyValue = getPropertyValue(DEV_INITIAL_UNIV_NUMBER);
		return propertyValue;

	}

	public Boolean getUniversitynumberAllowed() {
		String propertyValue = getPropertyValue(ALLOW_UNIVERISTY_NUMBER_INPUT);
		if (propertyValue == null) {
			log.error("Configuration for 'ALLOW.UNIVERSITY.NUMBER.INPUT' is missing - default to not allowed");
		} else if (propertyValue.trim().toLowerCase().equals("true")) {
			return true;
		}
		return false;
	}

	public String getDocumentServiceUserGroup() {
		if (Strings.isNullOrEmpty(documentServiceUserGroup)) {
			documentServiceUserGroup = getPropertyValue(Document_Service_User_Group);
		}
		return documentServiceUserGroup;
	}

	public String getDocumentServiceUserPassword() {
		if (Strings.isNullOrEmpty(documentServiceUserPassword)) {
			documentServiceUserPassword = getPropertyValue(Document_Service_User_Password);
		}
		return documentServiceUserPassword;
	}

	public String getAdminGroup() {
		if (Strings.isNullOrEmpty(adminGroup)) {
			adminGroup = getPropertyValue(ADMIN_GROUP);
		}

		return adminGroup;
	}

	public String getDIY_UrlEN() {
		String propertyValue = getPropertyValue(DIY_ENG_URL);
		if (propertyValue == null) {
			throw new VaadinUIException("error.config.is.missing", true);
		}
		return propertyValue;
	}

	public String getDIY_UrlAF() {
		String propertyValue = getPropertyValue(DIY_AFR_URL);
		if (propertyValue == null) {
			throw new VaadinUIException("error.config.is.missing", true);
		}
		return propertyValue;
	}
}
