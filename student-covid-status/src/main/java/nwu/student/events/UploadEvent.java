package nwu.student.events;

import nwu.student.ui.components.UploadComponent.FileReceiver;
import nwu.student.ui.dto.OnbaseContainer;

public class UploadEvent {
    private OnbaseContainer onbaseContainer;
    private String documentTypeKey;

    public UploadEvent(OnbaseContainer onbaseContainer, String docType) {
        this.onbaseContainer = onbaseContainer;
        this.documentTypeKey = docType;
    }

    public OnbaseContainer getOnbaseContainer() {
        return onbaseContainer;
    }

    public void setOnbaseContainer(OnbaseContainer onbaseContainer) {
        this.onbaseContainer = onbaseContainer;
    }

	public String getDocumentTypeKey() {
		return documentTypeKey;
	}
    
}
